-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2017 at 07:19 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_health`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_foodchart`
--

CREATE TABLE `tbl_foodchart` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `calorie` varchar(255) NOT NULL,
  `vitamin` varchar(255) NOT NULL,
  `fat` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_foodchart`
--

INSERT INTO `tbl_foodchart` (`id`, `name`, `calorie`, `vitamin`, `fat`, `category`) VALUES
(1, 'Test food 1', '30', '10', '0.5', 'breakfast'),
(2, 'Test food 2', '50', '30', '1', 'lunch'),
(3, 'Test food 3', '15', '5', '0.7', 'dinner');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tips`
--

CREATE TABLE `tbl_tips` (
  `id` int(11) NOT NULL,
  `body` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tips`
--

INSERT INTO `tbl_tips` (`id`, `body`) VALUES
(1, 'Don\'t Drink Sugar Calories'),
(2, 'Eat Nuts'),
(3, 'Avoid Processed Junk Food (Eat Real Food Instead)'),
(4, 'Don\'t Fear Coffee'),
(5, 'Eat Fatty Fish'),
(6, 'Get Enough Sleep'),
(7, 'Take Care of Your Gut Health With Probiotics and Fiber'),
(8, 'Drink Some Water, Especially Before Meals'),
(9, 'Don\'t Overcook or Burn Your Meat'),
(10, 'Avoid Bright Lights Before Sleep'),
(11, 'Eat Vegetables and Fruits'),
(12, 'Make Sure to Eat Enough Protein'),
(13, 'Do Some Cardio, or Just Walk More'),
(14, 'Don\'t Smoke or do Drugs, and Only Drink in Moderation'),
(15, 'Use Extra Virgin Olive Oil'),
(16, 'Minimize Your Intake of Added Sugars'),
(17, 'Don\'t Eat a Lot of Refined Carbohydrates'),
(18, 'Don\'t Fear Saturated Fat'),
(19, 'Lift Heavy Things'),
(20, 'Avoid Artificial Trans Fats'),
(21, 'Use Plenty of Herbs and Spices'),
(22, 'Take Care of Your Relationships'),
(23, 'Track Your Food Intake Every Now and Then'),
(24, 'If You Have Excess Belly Fat, Get Rid of it'),
(25, 'Don\'t go on a \"Diet\"'),
(26, 'Eat Eggs, and Don\'t Throw Away The Yolk'),
(27, 'Eat lots of fruit and veg'),
(28, 'Eat more fish – including a portion of oily fish'),
(29, 'Cut down on saturated fat and sugar'),
(30, 'Eat less salt – no more than 6g a day for adults');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `height` varchar(255) NOT NULL,
  `weight` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `username`, `gender`, `age`, `height`, `weight`, `email`, `password`) VALUES
(1, 'S Ahmed Naim', 'Naim', 'male', 23, '174', 55, 'naim.ahmed035@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(2, 'Alamgir Hossain', 'Alamgir', 'male', 30, '170', 50, 'alamgir@yahoo.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(3, 'Rabeya Jannat', 'Rabeya', 'female', 24, '167', 60, 'rabeya@pocha.manobi', '827ccb0eea8a706c4c34a16891f84e7b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_foodchart`
--
ALTER TABLE `tbl_foodchart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tips`
--
ALTER TABLE `tbl_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_foodchart`
--
ALTER TABLE `tbl_foodchart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_tips`
--
ALTER TABLE `tbl_tips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
