<?php
	include 'lib/User.php';
	include 'inc/header.php';
	Session::checkSession();
?>

<?php
	if (isset($_GET['id'])) {
		$userid = (int)$_GET['id'];
		$sesId = Session::get("id");
		/*
		if ($userid != $sesId) {
			header("Location: index.php");
		}
		*/
	}

	$user = new User();
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])) {
		$updateUser = $user->updateUserData($userid, $_POST);
	}
?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>
					User Profile 
					<span class="pull-right"><a class="btn btn-primary" href="index.php">Back</a></span>
				</h2>
			</div>

			<div class="panel-body">
				<div style="max-width: 600px; margin: 0px auto;">
<?php 
	if (isset($updateUser)) {
		echo $updateUser;
	}
?>

<?php 
	$userdata = $user->getUserById($userid);
	if ($userdata) {

?>
					<form action="" method="post">
						<div class="form-group">
							<label for="name">Your Name</label>
							<input type="text" id="name" name="name" class="form-control" value="<?php echo $userdata->name; ?>" />
						</div>
						
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" id="username" name="username" class="form-control" value="<?php echo $userdata->username; ?>" />
						</div>

						<div class="form-group">
							<label for="gender">Gender</label>
							<select class="form-control" id="gender" name="gender">
							    <option value="">Select your gender</option>
							    <option value="male" 
									<?php
										if ($userdata->gender == "male") {
											echo "selected";
										}
									?>
							    >Male</option>
							    <option value="female" 
									<?php
										if ($userdata->gender == "female") {
											echo "selected";
										}
									?>
							    >Female</option>
							 </select>
						</div>

						<div class="form-group">
							<label for="age">Age</label>
							<input type="number" id="age" name="age" class="form-control" value="<?php echo $userdata->age; ?>" />
						</div>

						<div class="form-group">
							<label for="height">Height</label>
							<input type="number" id="height" name="height" class="form-control" value="<?php echo $userdata->height; ?>" />
						</div>

						<div class="form-group">
							<label for="weight">Weight</label>
							<input type="number" id="weight" name="weight" class="form-control" value="<?php echo $userdata->weight; ?>" />
						</div>

						<div class="form-group">
							<label for="email">Email Address</label>
							<input type="text" id="email" name="email" class="form-control" value="<?php echo $userdata->email; ?>" readonly />
						</div>
						
						<?php
							$sesId = Session::get("id");
							if ($userid == $sesId) {
						?>
						<button type="submit" name="update" class="btn btn-success">Update</button>
						<a class="btn btn-info" href="changepass.php?id=<?php echo $userid; ?>">Password Change</a>
						<?php 
							}
						?>
					</form>
<?php 
	}
?>
				</div>
			</div>
		</div>

<?php
	include 'inc/footer.php';
?>