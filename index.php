<?php
	include 'lib/User.php';
	include 'inc/header.php';
	Session::checkSession();
?>

<style type="text/css">
	table, tr, th, td {
		text-align: center;
	}
</style>

<?php
	$loginmsg = Session::get("loginmsg");
	if (isset($loginmsg)) {
		echo $loginmsg;
	}
	Session::set("loginmsg", NULL);
?>

<?php
	$user = new User();
	$id = rand(1,30);
	$tips = $user->healthTips($id);
?>

<div class='alert alert-info'>
	<strong>Tips! </strong><?php echo $tips; ?>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h2>
			Food chart 
			<span class="pull-right">Welcome! <strong>
				<?php 
					$name = Session::get("username");
					if (isset($name)) {
						echo $name;
					}
				?>
			</strong></span>
		</h2>
	</div>
	<div class="panel-body">
		<table class="table table-striped">
			<tr>
				<th width="20%">Serial</th>
				<th width="20%">Food Name</th>
				<th width="20%">Calorie</th>
				<th width="20%">Vitamin</th>
				<th width="20%">Fat</th>
			</tr>

<?php
	$foodList = $user->getFoodList();
	if ($foodList) {
		$i = 0;
		foreach ($foodList as $foods) {
			$i++; ?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $foods['name']; ?></td>
				<td><?php echo $foods['calorie']; ?></td>
				<td><?php echo $foods['vitamin']; ?></td>
				<td><?php echo $foods['fat']; ?></td>
			</tr>
		<?php }
	}
?>


		</table>
	</div>
</div>

<?php
	include 'inc/footer.php';
?>