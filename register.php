<?php
	include 'inc/header.php';
	include 'lib/User.php';
?>

<?php
	$user = new User();
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
		$userRegistration = $user->userRegistration($_POST);
	}

?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>User Registration</h2>
			</div>

			<div class="panel-body">
				<div style="max-width: 600px; margin: 0px auto;">

<?php
	if (isset($userRegistration)) {
		echo $userRegistration;
	}

?>

					<form action="" method="post">
						<div class="form-group">
							<label for="name">Your Name</label><span class="required_field">*</span>
							<input type="text" id="name" name="name" class="form-control" placeholder="Enter name here..." />
						</div>
						
						<div class="form-group">
							<label for="username">Username</label><span class="required_field">*</span>
							<input type="text" id="username" name="username" class="form-control" placeholder="Enter username here..." />
						</div>

						<div class="form-group">
							<label for="gender">Gender</label><span class="required_field">*</span>
							<select class="form-control" id="gender" name="gender">
							    <option value="">Select your gender</option>
							    <option value="male">Male</option>
							    <option value="female">Female</option>
							 </select>
						</div>

						<div class="form-group">
							<label for="age">Age</label><span class="required_field">*</span>
							<input type="number" id="age" name="age" class="form-control" placeholder="Enter age here..." />
						</div>

						<div class="form-group">
							<label for="height">Height (in Inches)</label>
							<input type="number" id="height" name="height" class="form-control" placeholder="Enter height here..." />
						</div>

						<div class="form-group">
							<label for="weight">Weight</label>
							<input type="number" id="weight" name="weight" class="form-control" placeholder="Enter weight here..." />
						</div>

						<div class="form-group">
							<label for="email">Email Address</label><span class="required_field">*</span>
							<input type="text" id="email" name="email" class="form-control" placeholder="Enter email address here..." />
						</div>

						<div class="form-group">
							<label for="password">Password</label><span class="required_field">*</span>
							<input type="password" id="password" name="password" class="form-control" placeholder="Enter a password here..." />
						</div>
						<button type="submit" name="register" class="btn btn-default">Register</button>
					</form>

				</div>
			</div>
		</div>

<?php
	include 'inc/footer.php';
?>